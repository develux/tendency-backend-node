var express = require('express');
var router = express.Router();
var axios = require('axios');

let envios = 0;
let guias = [];

router.get('/listar-guias', async function (req, res, next) {
    res.send({
        info: guias,
        total: envios
    });
});

router.post('/nueva-guia', async function (req, res, next) {
    const peticion = await axios.post('https://api-test.envia.com/ship/generate/', {
        "origin": {
            "name": "oscar mx",
            "company": "oskys factory",
            "email": "osgosf8@gmail.com",
            "phone": "8116300800",
            "street": "av vasconcelos",
            "number": "1400",
            "district": "mirasierra",
            "city": "Monterrey",
            "state": "NL",
            "country": "MX",
            "postalCode": "66236",
            "reference": ""
        },
        "destination": {
            "name": "oscar",
            "company": "empresa",
            "email": "osgosf8@gmail.com",
            "phone": "8116300800",
            "street": "av vasconcelos",
            "number": "1400",
            "district": "palo blanco",
            "city": "monterrey",
            "state": "NL",
            "country": "MX",
            "postalCode": "66240",
            "reference": ""
        },
        "packages": [
            {
                "content": "camisetas rojas",
                "amount": 2,
                "type": "box",
                "dimensions": {
                    "length": 2,
                    "width": 5,
                    "height": 5
                },
                "weight": 63,
                "insurance": 0,
                "declaredValue": 400,
                "weightUnit": "KG",
                "lengthUnit": "CM"
            },
            {
                "content": "camisetas rojas",
                "amount": 2,
                "type": "box",
                "dimensions": {
                    "length": 1,
                    "width": 17,
                    "height": 2
                },
                "weight": 5,
                "insurance": 400,
                "declaredValue": 400,
                "weightUnit": "KG",
                "lengthUnit": "CM"
            }
        ],
        "shipment": {
            "carrier": "fedex",
            "service": "express",
            "type": 1
        },
        "settings": {
            "printFormat": "PDF",
            "printSize": "STOCK_4X6",
            "comments": "comentarios de el envío"
        }

    }, {
        headers: {
            'Authorization': 'Bearer d6a80e47c592483fcc169c8e94acf3416b4193e1da420799dfc515aa3ad68054'
        }
    })


    // >>COMENTAR BLOQUE EN PRODUCCIÓN
    let guia = {
        "carrier": "fedex",
        "service": "express",
        "trackingNumber": "788150569365",
        "trackUrl": "https://envia.com/rastreo?label=788150569365&cntry_code=mx",
        "label": "https://s3.us-east-2.amazonaws.com/enviapaqueteria/uploads/fedex/788150569365_fedex.pdf",
        "currentBalance": 466,
        "currency": "MXN"
    };

    envios = parseInt(envios) + parseInt(1)
    guias.unshift(guia)
    let data = [
        {
            info: guia,
            total: envios
        }
    ];
    ios.emit('nueva-guia', data);
    res.send("ok");
    // <<FIN DEL BLOQUE


    // >>DESCOMENTAR CLOQUE EN PRODUCCIÓN
    /*if (peticion) {
        let data = peticion.data;
        if (data) {
            if (data.meta == 'error') {
                res.status(400).send("Hubo un error al intentar crear la guía");
            } else {
                let data = [
                    {
                        info: peticion.data,
                        total: envios
                    }
                ];
                ios.emit('nueva-guia', data);
                res.status(200).send(data);
            }
        } else {
            res.status(500).send("Error interno");
        }
    } else {
        res.status(500).send("Error interno");
    }*/
    // <<FIN DEL BLOQUE
});


router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});

module.exports = router;
